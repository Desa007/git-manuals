# git-manuals


## Work with branchs
**git branch** ***-a*** - list all local and remote branches

**git branch -M main** - rename your default branch name to "main"

## Work with commits
**git add . && git commit -m "new project"** = **git commit -am "new project"** - create "new project" commit message

**git commit --amend -m "Love"** - rename your current commit message

**git add . && git commit --amend --no-edit** - include additional changes to the current commit before pushing it to the remote repository

## Revert commit
**git revert <commit_ID> - To revert to a previous commit. This creates a new commit with the changes from the previous commit.

## git reset
**git reset HEAD** ***<file_name>*** - remove file from index

## git checkout
**git checkout** ***<file_name>*** - drop all changes for file

**git checkout** ***<branch_name>*** - switch to branch

**git checkout** ***<branch_name>*** - switch to branch

**git checkout HEAD^** - move up to previous commit in current branch

**git checkout** ***<branch_name> <hash_commit>*** - switch to branch at hash_commit

**git checkout -b** ***<branch_name>*** - create new branch and switch to it

**git checkout -b** ***<branch_name> <hash-commit>*** - create new branch from <hash-commit> and switch to it

***
## git merge
**git merge** ***<origin_branch>*** - megre current_branch with origin_branch and save all commit history

**git merge --squash** ***<origin_branch>*** - do the previous but replace all commit history in *<origin_branch>* into one commit

```git
// All commit history from devops/ci-cd branch will added to current branch
git merge devops/ci-cd

// Only our "Last commit" will added into current brach
git merge --squash devops/ci-cd
git commit -m "Merge branch devops/ci-cd with current branch"
```

## git rebase
**git rebase** ***-i HEAD~5*** - change 5 latest commits.
To squash latest commits into one use "squash" lable for commit in interactive mode.
**git rebase** ***--abord*** - stop rebasing.

**git rebase** ***--continue*** - continue rebasing after resolving conflicts.
## Examples

### Most common usage
```git
git status
git add .
git commit -m "<commit description>"
git push origin main
```

### Add your files
```
cd existing_repo
git remote add origin https://gitlab.com/Desa007/git-manuals.git
git branch -M main
git push -uf origin main
```


***
# Useful references
- [Git official documentation](https://git-scm.com/)
- [Git simulator](https://learngitbranching.js.org/)
